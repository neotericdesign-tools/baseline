# Baseline

Neoteric's fundamental theme for our client projects. 

### Menuing

Add this to FM on content pages to activate the main menu: 
```
[menu.main]
name = "Contact"
weight = 7
```

**What is the proper way to tag new releases?**

Semantic versioning divides releases into:

* a major (breaking) version,
* a minor (feature) version,
* and a patch version.

The full version is prepended with a single “v” and looks like “v1.2.3”, which translates into a major version 1, with minor version 2 and patch version 3.


**for bug fixes that aren't new features, we do 2.0.0, etc.?**

When there is a bugfix the patch version is increased. So 2.0.1


**we continue with 2.1, 2.2, to handle new features?**

yes


**do versions live on branches?**

The latest version should be on the master branch and previous version have their own branches in case of feature or patches


**How would we do a bugfix on v1.1.1, without breaking v2?**

Because of semantic versioning, it is assumed v2 is incompatible with v1. Therefore, bugfixes to v1 should not affect projects using v2.
Can we, for older client projects, say: "baseline v1", and it picks up all of v1, v1.1.1, v1.5..


> Naming Go’s selection algorithm “minimal version selection” is a bit of a misnomer, but once you learn how it works you will see the name comes really close. As I stated before, many selection algorithms select the “latest greatest” version of a dependency. I like to think of MVS as an algorithm that selects the “latest non-greatest” version. It’s not that MVS can’t select the “latest greatest”, it’s just that if the “latest greatest” is not required by any dependency in the project, that version isn’t needed. 
> If your module depends on A that itself has a require D v1.0.0 and your module also depends on B that has a require D v1.1.1, then Go Modules would select v1.1.1 of dependency D. This selection of v1.1.1 remains consistent even if some time later a v1.2.0 of D becomes available.
REFERENCE: https://www.ardanlabs.com/blog/2019/12/modules-03-minimal-version-selection.html
